function findById(arr,findId){
        const empty = [];
        if(!Array.isArray(arr)) return empty;
        if(arr.length==0) return empty;
        if(typeof findId !== 'number') return empty;
        

        for(let item=0; item<arr.length; item++){
            if(arr[item].id===findId){
                return arr[item];
            } 
        }
    }
module.exports = findById;





