function onlyBMWAndAudi(arr){
    const BMWAndAudi=[];
    for(let item=0 ;item<arr.length; item++){
        if(arr[item].car_make === "Audi" || arr[item].car_make === "BMW"){
            BMWAndAudi.push(arr[item]);
        }
    }
    return BMWAndAudi;
}

module.exports = onlyBMWAndAudi;